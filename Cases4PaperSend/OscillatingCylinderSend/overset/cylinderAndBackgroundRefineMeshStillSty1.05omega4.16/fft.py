import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack

def main():
    N = 1000
    forceFile = "postProcessing/force.dat"
    forces = np.genfromtxt(forceFile,comments="#")
    times = forces[:,0]
    forceX = forces[:,1]
    forceY = forces[:,2]
    forceZ = forces[:,3]
    t = np.linspace(times[0],times[-1],N)
    forzZint = np.interp(t, times, forceZ)
    fftFz = fftpack.fft(forzZint[int(N/2)-1:N-1])
    #Y    = numpy.fft.fft(y)
    freq = fftpack.fftfreq(forzZint[int(N/2)-1:N-1].size, d=t[1]-t[0]) 
    #freq = np.fft.fftfreq(t[int(N/2):N-1].shape[-1])
    #plt.plot(freq, fftFz.real, freq, fftFz.imag)
    plt.plot(t[int(N/2):N-1], forzZint[int(N/2):N-1])
    plt.savefig('Lift.png')
    plt.close()
    plt.plot(freq[0:freq.size/2], np.abs(fftFz)[0:fftFz.size/2])
    plt.savefig('fftLift.png')
    plt.close()
    #np.savetxt("fftLift.txt",np.transpose([freq,np.abs(fftFz)]))
    print "frequency with the max power"
    print freq[np.argmax(np.abs(fftFz))]
    
    
if __name__== "__main__":
  main()
    
