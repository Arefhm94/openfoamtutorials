# importing re module for creating

# regex expression

import re
import numpy as np
from io import StringIO
import matplotlib.pyplot as plt


fileNameForce = "postProcessing/force.dat"
fileNameMoment = "postProcessing/moment.dat"

fileNameForce1 = "postProcessing/forces1/0/force.dat"
fileNameMoment1 = "postProcessing/forces1/0/moment.dat"

Clfile = "../Clkappa0dot1.txt"
Cdfile = "../Cdkappa0dot1.txt"
Cmfile = "../Cmkappa0dot1.txt"

ClfileSt = "../Clstatic.txt"
CdfileSt = "../Cdstatic.txt"
CmfileSt = "../Cmstatic.txt"

amplitude = 15.0
omega = 0.27
alphaStart = 10.0

Ux = 1.33
Uz = 0.23
UmagSq = Ux*Ux + Uz*Uz
L = 1
H = 0.05



def FPvectorReader(fileName):

    #with open(fileName, 'r') as f:
              ## looping the para and iterating
              ## each line
        #text = f.read()
              ## getting the pattern for [],(),{}
              ## brackets and replace them to empty
              ## string
              ## creating the regex pattern & use re.sub()
        #patn = re.sub(r"[\([{})\]]", "", text)
        
        ##print(patn)

 
    array = np.genfromtxt(fileName,comments="#")
    
    
    tarray=  (array[:,0])
    FxTot =  (array[:,1])
    FyTot =  (array[:,2])
    FzTot =  (array[:,3])
    FxPre =  (array[:,4])
    FyPre =  (array[:,5])
    FzPre =  (array[:,6])
    FxVis =  (array[:,7])
    FyVis =  (array[:,8])
    FzVis =  (array[:,9])

    return tarray,FxTot,FyTot,FzTot,FxPre,FyPre,FzPre,FxVis,FyVis,FzVis





if __name__ == "__main__":


    tarray,FxTot,FyTot,FzTot,FxPre,FyPre,FzPre,FxVis,FyVis,FzVis = FPvectorReader(fileNameForce)
    
    tarray1,MxTot,MyTot,MzTot,MxPre,MyPre,MzPre,MxVis,MyVis,MzVis = FPvectorReader(fileNameMoment)
    
    Clarray = np.genfromtxt(Clfile,comments="#",delimiter=",")
    Cdarray = np.genfromtxt(Cdfile,comments="#",delimiter=",")
    Cmarray = np.genfromtxt(Cmfile,comments="#",delimiter=",")
    
    ClarraySt = np.genfromtxt(ClfileSt,comments="#",delimiter=",")
    CdarraySt = np.genfromtxt(CdfileSt,comments="#",delimiter=",")
    CmarraySt = np.genfromtxt(CmfileSt,comments="#",delimiter=",")
    
    alpha = alphaStart + amplitude*np.sin(omega*tarray)

    Fptot = np.sqrt(np.square(FzPre),np.square(FxPre))
    
    alpharad = alphaStart*np.pi/180
    
    Fdrag = np.subtract(np.multiply(FxPre,
            np.cos(-alpharad)),np.multiply(FzPre,np.sin(-alpharad)))
    Flift = np.add(np.multiply(FxPre,
            np.sin(-alpharad)),np.multiply(FzPre,np.cos(-alpharad)))

    fig, ax = plt.subplots()
    ax.set_ylim(-0.5,2.5)
    ax.plot(alpha,2*Flift/H/L/UmagSq,color='b',label='sim',linewidth=1)
    ax.plot(Clarray[:,0],Clarray[:,1],color='r',label='exp dynamic')
    ax.plot(ClarraySt[:,0],ClarraySt[:,1],color='m',label='exp static')
    ax.set(xlabel='alpha [deg]', ylabel='Cl [-]',
                     title='Cl plot')
    ax.legend()
    fig.savefig("Cl0dot1kO.png")
    
    fig, ax = plt.subplots()
    ax.set_ylim(-.1,1)
    ax.plot(alpha,2*Fdrag/H/L/UmagSq,color='b',label='sim',linewidth=1)
    ax.plot(Cdarray[:,0],Cdarray[:,1],color='r',label='exp dynamic')
    ax.plot(CdarraySt[:,0],CdarraySt[:,1],color='m',label='exp static')
    ax.set(xlabel='alpha [deg]', ylabel='Cd [-]',
                     title='Cd plot')
    ax.legend()
    fig.savefig("Cd0dot1kO.png")
    
    fig, ax = plt.subplots()
    ax.set_ylim(-0.1,0.04)
    ax.plot(alpha,2*MyPre/H/L/UmagSq,color='b',label='sim',linewidth=1)
    ax.plot(Cmarray[:,0],Cmarray[:,1],color='r',label='exp dynamic')
    ax.plot(CmarraySt[:,0],CmarraySt[:,1],color='m',label='exp static')
    ax.set(xlabel='alpha [deg]', ylabel='Cm [-]',
                     title='Cm plot')
    ax.legend()
    fig.savefig("Cm0dot1kO.png")



 


