import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os
import math

timeStep = 0.22

pInfty = 100000
rhoInfty = 1.17
Uinfty = math.sqrt(250.5**2  + 17.97**2)

def get_variable(folderName,timeStep,fileName):
    fileToProcess = os.path.join(folderName,"postProcessing/samplePwall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    z = array[:,2]
    var = array[:,3]
    
    return x,z,var
    
    

def main():
    
    exp = np.genfromtxt("cpset6.csv",comments="#",delimiter=',')
    simCp = np.genfromtxt("../2012IovnovichCpMa0.725alpha4deg.csv",comments="#",delimiter=',')
    simCf = np.genfromtxt("../2012IovnovichCfMa0.725alpha4deg.csv",comments="#",delimiter=',')
    folderList = ["realizableKE" , "kOmega" , "kOmegaSST" , "SpalartAllmaras"]
            
    fileNameList = ["p_patch_aerofoil.raw" , "wallShearStress_patch_aerofoil.raw", "yPlus_patch_aerofoil.raw"]
    
    varNameList = ["Cp","Cf","yPlus"]
    
    colorList = ["b", "g", "r", "k"]
    
    figCp = plt.figure()
    axCp = figCp.add_subplot(1, 1, 1)
    axCp.set_xlabel("x/c [-]")
    axCp.set_ylabel("Cp [-]")

    figCf = plt.figure()
    axCf = figCf.add_subplot(1, 1, 1)
    axCf.set_xlabel("x/c [-]")
    axCf.set_ylabel("Cf [-]")
        
    figYp = plt.figure()
    axYp = figYp.add_subplot(1, 1, 1)
    axYp.set_xlabel("x/c [-]")
    axYp.set_ylabel("yPlus [-]")
    
    figList = [figCp,figCf,figYp]
    axList = [axCp,axCf,axYp]

    
    for varName,fileName,fig,ax in zip(varNameList,fileNameList,figList,axList):
        for folderName,colorP in zip(folderList,colorList):
            x,z,var = get_variable(folderName,timeStep,fileName)
            
            if varName == "Cp": var = -np.divide(var-pInfty,0.5*rhoInfty*Uinfty**2)
            if varName == "Cf": var = np.divide(-var,0.5*rhoInfty*Uinfty**2)
            
            varUp = var[z>0]
            varLo = var[z<0]
            xUp = x[z>0]
            xLo = x[z<0]
            
            xUpis = xUp.argsort()
            sorted_xu = xUp[xUpis]
            sorted_vu = varUp[xUpis]
            xLois = xLo.argsort()
            sorted_xl = xLo[xLois]
            sorted_vl = varLo[xLois]

            
            ax.plot(sorted_xl, sorted_vl, color=colorP,label=folderName,linewidth=1)
            ax.plot(sorted_xu, sorted_vu, color=colorP,linewidth=1)
            
        if varName == "Cp":
            ax.plot(exp[:,0],exp[:,1],'bo',label="exp")
            ax.plot(simCp[:,0],simCp[:,1],'rv',label="ref sim")
        if varName == "Cf":
            ax.plot(simCf[:,0],simCf[:,1],'rv',label="ref sim")
        
        ax.legend()
            
            
        fig.savefig(varName+'turbulenceModel'+'.png')
        
    
    
    
if __name__== "__main__":
  main()
    
