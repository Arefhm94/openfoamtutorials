import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os

N = 10000
Nev = 3000
U = 1
D = 1
H = 0.1

def get_t_forceX_forceZ_(folderName):
    forceFile = os.path.join(folderName,"postProcessing/force.dat")
    forces = np.genfromtxt(forceFile,comments="#")
    times = forces[:,0]
    forceX = forces[:,1]
    forceY = forces[:,2]
    forceZ = forces[:,3]
    t = np.linspace(times[0],times[-1],N)
    forceZint = np.interp(t, times, forceZ)
    forceXint = np.interp(t, times, forceX)
    
    return t,forceXint,forceZint
    

def main():
    

    
    folderList = ["cylinderMotionOmega0" , "cylinderMotionOmega1.04" , "cylinderMotionOmega2.08" , "cylinderMotionOmega4.16"]
    
    fcList = [0,0.165, 0.33, 0.66]
    
    colorList = ["b", "g", "r", "k"]
    
    figCl = plt.figure()
    axCl = figCl.add_subplot(1, 1, 1)
        
    figCd = plt.figure()
    axCd = figCd.add_subplot(1, 1, 1)
        
    figFr = plt.figure()
    axFr = figFr.add_subplot(1, 1, 1)
    
    for folderName,fc,colorP in zip(folderList,fcList,colorList):
                
        print "Folder analyzed: ",folderName
        
        t,forceXint,forceZint = get_t_forceX_forceZ_(folderName)
    
        fftFz = fftpack.fft(forceZint[Nev-1:N-1])
        freq = fftpack.fftfreq(forceZint[Nev-1:N-1].size, d=t[1]-t[0]) 
                
        axCl.plot(t, forceZint/(0.5*U*U*D*H), color=colorP,label=r'$f_c =$ '+str(fc)+r'$\frac{1}{s}$',linewidth=1)
        axCl.legend()
        axCl.set_ylim(-1,1)
        axCl.set_xlim(180,200)
        axCl.set_xlabel("t [s]")
        axCl.set_ylabel("Cl [-]")
        
        axCd.plot(t, forceXint/(0.5*U*U*D*H), color=colorP,label=r'$f_c =$ '+str(fc)+r'$\frac{1}{s}$',linewidth=1)
        axCd.legend()
        axCd.set_ylim(-3.5,6.5)
        axCd.set_xlim(180,200)
        axCd.set_xlabel("t [s]")
        axCd.set_ylabel("Cd [-]")
        
        axFr.plot(freq[0:freq.size/2],  np.abs(fftFz)[0:fftFz.size/2], color=colorP,label=r'$f_c =$ '+str(fc)+r'$\frac{1}{s}$',linewidth=1)
        axFr.legend()
        axFr.set_xlabel("fr [1/s]")
        axFr.set_xlim(0,2)
        axFr.set_ylabel("Power")
        
        #np.savetxt("fftLift.txt",np.transpose([freq,np.abs(fftFz)]))
        print "Strouhal number for the frequency with the maximum power"
        print freq[np.argmax(np.abs(fftFz))]
        
        print "Mean drag coefficient"
        print np.mean(forceXint[Nev:N])/(0.5*U*U*D*H)
        
        print "Max lift coefficient"
        print np.max(forceZint[Nev:N])/(0.5*U*U*D*H)
  
    figCl.savefig('ClAllMorphing.png')
    figCd.savefig('CdAllMorphing.png')
    figFr.savefig('FrAllMorphing.png')
    
if __name__== "__main__":
  main()
    
