set term png


set output 'Cdtot.png'
set xr [0.015:0.035]
set xlabel 't'
set ylabel 'Cd tot'

plot "< sed s/[\\(\\)]//g baseMesh/postProcessing/forces1/0/force.dat" using 1:(2*$2/(1.19*411**2*2.426E-11)) w l tit 'mesh coars', "< sed s/[\\(\\)]//g baseMeshFine/postProcessing/forces1/0/force.dat" using 1:(2*$2/(1.19*411**2*2.426E-11)) w l tit 'mesh fine', "< sed s/[\\(\\)]//g refineTestsDeltaPStartFromBase/postProcessing/forces1/0/force.dat" using 1:(2*$2/(1.19*411**2*2.426E-11)) w l tit 'AMR plim 7000', "< sed s/[\\(\\)]//g refineTestsDeltaPStartFromBase2/postProcessing/forces1/0/force.dat" using 1:(2*$2/(1.19*411**2*2.426E-11)) w l  tit 'AMR plim 5000'


set output 'Cdp.png'
set xr [0.015:0.035]
set xlabel 't'
set ylabel 'Cd p'

plot "< sed s/[\\(\\)]//g baseMesh/postProcessing/forces1/0/force.dat" using 1:(2*$5/(1.19*411**2*2.426E-11)) w l tit 'mesh coars', "< sed s/[\\(\\)]//g baseMeshFine/postProcessing/forces1/0/force.dat" using 1:(2*$5/(1.19*411**2*2.426E-11)) w l tit 'mesh fine', "< sed s/[\\(\\)]//g refineTestsDeltaPStartFromBase/postProcessing/forces1/0/force.dat" using 1:(2*$5/(1.19*411**2*2.426E-11)) w l tit 'AMR plim 7000', "< sed s/[\\(\\)]//g refineTestsDeltaPStartFromBase2/postProcessing/forces1/0/force.dat" using 1:(2*$5/(1.19*411**2*2.426E-11)) w l  tit 'AMR plim 5000'

set output 'Cdvis.png'
set xr [0.015:0.035]
set xlabel 't'
set ylabel 'Cd vis'

plot "< sed s/[\\(\\)]//g baseMesh/postProcessing/forces1/0/force.dat" using 1:(2*$8/(1.19*411**2*2.426E-11)) w l tit 'mesh coars', "< sed s/[\\(\\)]//g baseMeshFine/postProcessing/forces1/0/force.dat" using 1:(2*$8/(1.19*411**2*2.426E-11)) w l tit 'mesh fine', "< sed s/[\\(\\)]//g refineTestsDeltaPStartFromBase/postProcessing/forces1/0/force.dat" using 1:(2*$8/(1.19*411**2*2.426E-11)) w l tit 'AMR plim 7000', "< sed s/[\\(\\)]//g refineTestsDeltaPStartFromBase2/postProcessing/forces1/0/force.dat" using 1:(2*$8/(1.19*411**2*2.426E-11)) w l  tit 'AMR plim 5000'



set output 'p.png'
set xlabel 'x/R'
set ylabel 'p [Pa]'
set xr [-4:2]
set grid

plot  'baseMesh/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):3 w l  tit 'mesh coars',   'baseMeshFine/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):3 w l tit 'mesh fine', 'refineTestsDeltaPStartFromBase/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):3 w l tit 'AMR plim 7000','refineTestsDeltaPStartFromBase2/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):3 w l tit 'AMR plim 5000'

set output 'T.png'
set xlabel 'x/R'
set ylabel 'T [K]'
set xr [-4:2]
set grid

plot  'baseMesh/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):2 w l  tit 'mesh coars',   'baseMeshFine/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):2 w l tit 'mesh fine', 'refineTestsDeltaPStartFromBase/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):2 w l tit 'AMR plim 7000','refineTestsDeltaPStartFromBase2/postProcessing/Graphs/0.035/line_T_p.xy' u (2*$1/1.134e-05):2 w l tit 'AMR plim 5000'

set output 'U.png'
set xlabel 'x/R'
set ylabel 'U [m/s]'
set xr [-2:4.5]
set grid

plot  'baseMesh/postProcessing/Graphs/0.035/line_U.xy' u (2*$1/1.134e-05):2 w l  tit 'mesh coars',   'baseMeshFine/postProcessing/Graphs/0.035/line_U.xy' u (2*$1/1.134e-05):2 w l tit 'mesh fine', 'refineTestsDeltaPStartFromBase/postProcessing/Graphs/0.035/line_U.xy' u (2*$1/1.134e-05):2 w l tit 'AMR plim 7000','refineTestsDeltaPStartFromBase2/postProcessing/Graphs/0.035/line_U.xy' u (2*$1/1.134e-05):2 w l tit 'AMR plim 5000'
