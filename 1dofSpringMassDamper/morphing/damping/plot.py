import numpy as np
import matplotlib.pyplot as plt
import os

D = 0.0016

def get_t_Z(folderName):
    File = os.path.join(folderName,"plotfile")
    displacement = np.genfromtxt(File,comments="#")
    times = displacement[:,0]
    Z = displacement[:,3]

    
    return times,Z
        



def main():
    
    folderList = ["cylindermoveMeshOuterCorrectorsNo" , "cylindermoveMeshOuterCorrectorsYes" , "cylindermoveMeshOuterCorrectorsYes4Outer" , "cylindermoveMeshsymplectic"]
    
    fcList = ["Newmark no outer correction","Newmark 2 outer", "Newmark 4 outer", "symplectic"]
    
    colorList = ["b", "g", "r", "k"]
    
    figZ = plt.figure()
    axZ = figZ.add_subplot(1, 1, 1)
        

    
    for folderName,fc,colorP in zip(folderList,fcList,colorList):
                
        print "Folder analyzed: ",folderName
        
        t,Z = get_t_Z(folderName)
        
        if folderName == "cylindermoveMeshOuterCorrectorsNo":
            axZ.plot(t, np.exp(-0.5*0.0039/0.03575*t)*np.cos(np.sqrt(69.48/0.03575)*t), color="m",label="analytical solution",linewidth=1)
            axZ.legend()
            axZ.set_ylim(-3,3)
            axZ.set_xlabel("t [s]")
            axZ.set_ylabel("z/D [-]")
        
        axZ.plot(t, Z/D, color=colorP,label=fc,linewidth=1)
        axZ.legend()
        axZ.set_ylim(-3,3)
        axZ.set_xlabel("t [s]")
        axZ.set_ylabel("z/D [-]")
        
        
    
  
    figZ.savefig('zMorphingDamped.png')

    
if __name__== "__main__":
  main()
    
