set term png
set output 'cOG.png'
set xlabel 't[s]'
set ylabel 'cOG [m]'
set key left top
set parametric
set yr [0:0.018]

plot 'dx0.075/centerOfGravityAir.dat' u 1:2 w l tit 'dx = 0.075 mm', 'dx0.1/centerOfGravityAir.dat' u 1:2 w l tit 'dx = 0.1 mm', 'dx0.13/centerOfGravityAir.dat' u 1:2 w l tit 'dx = 0.13 mm',  'dx0.2/centerOfGravityAir.dat' u 1:2 w l tit 'dx = 0.2 mm', 0.684,t tit 't detachment experiment' lt 0
