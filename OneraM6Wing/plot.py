import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os
import math

timeStep = 500

pInfty = 100000
rhoInfty = 1.17
Uinfty = math.sqrt(290**2  + 13.73**2)
b = 1.1963
yList = [0.2, 0.44, 0.65, 0.8, 0.9, 0.96, 0.99]
binwdith = 0.0025*b

def get_variable(timeStep,fileName,yPos):
    fileToProcess = os.path.join("postProcessing/samplePwall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    y = array[:,1]
    z = array[:,2]
    var = array[:,3]
    
    x = x[np.abs(y-yPos)<binwdith]
    z = z[np.abs(y-yPos)<binwdith]
    var = var[np.abs(y-yPos)<binwdith]
    
    return x,z,var
    
    

def main():
    
    #exp = np.genfromtxt("cpset6.csv",comments="#",delimiter=',')
    #simCp = np.genfromtxt("../2012IovnovichCpMa0.725alpha4deg.csv",comments="#",delimiter=',')
    #simCf = np.genfromtxt("../2012IovnovichCfMa0.725alpha4deg.csv",comments="#",delimiter=',')
 
    
    folderName = ["./"]
     
     
    fileNameList = ["p_patch_wing.raw" , "wallShearStress_patch_wing.raw", "yPlus_patch_wing.raw"]
    
    varNameList = ["Cp","Cf","yPlus"]
    
    
    for varName,fileName in zip(varNameList,fileNameList):
        for y in yList:
            
            yPos = y*b
            
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlabel("x/c [-]")
            ax.set_ylabel(varName+" [-]")
            
            x,z,var = get_variable(timeStep,fileName,yPos)
            x=np.subtract(x,np.min(x))
            x=np.divide(x,np.max(x))
            
            if varName == "Cp": var = -np.divide(var-pInfty,0.5*rhoInfty*Uinfty**2)
            if varName == "Cf": var = np.divide(-var,0.5*rhoInfty*Uinfty**2)
            
            varUp = var[z>0]
            varLo = var[z<0]
            xUp = x[z>0]
            xLo = x[z<0]
            
            xUpis = xUp.argsort()
            sorted_xu = xUp[xUpis]
            sorted_vu = varUp[xUpis]
            xLois = xLo.argsort()
            sorted_xl = xLo[xLois]
            sorted_vl = varLo[xLois]

            
            ax.plot(sorted_xl, sorted_vl,color='r',label='y/b = '+str(y),linewidth=1)
            ax.plot(sorted_xu, sorted_vu,color='r',linewidth=1)
            
            if varName == "Cp":
                exp = np.genfromtxt("expy="+str(y)+".dat",comments="#")
                ax.plot(exp[:,2],-exp[:,4],'bo',label='exp')
                
            
        #if varName == "Cp":
            #ax.plot(exp[:,0],exp[:,1],'bo',label="exp")
            #ax.plot(simCp[:,0],simCp[:,1],'rv',label="ref sim")
        #if varName == "Cf":
            #ax.plot(simCf[:,0],simCf[:,1],'rv',label="ref sim")
        
            ax.legend()
            
            
            fig.savefig(varName+'y'+str(y).replace(".","_")+'.png')
            plt.close()
        
    
    
    
if __name__== "__main__":
  main()
    
